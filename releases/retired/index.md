---
title: Azure SDK Releases (Retired)
layout: default
sidebar: releases_sidebar
---
{% include releases/header.md type="retired" %}
{% include releases/nav.md %}
{% include releases/languages.md type="retired" %}
{% include refs.md %}